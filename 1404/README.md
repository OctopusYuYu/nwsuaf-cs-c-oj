## 编程题 1404 计算梯形面积
### 题目描述
已知梯形的上底、下底和高，计算梯形的面积。
### 输入
梯形的上底、下底和高。
### 输出
梯形的面积。
### 样例输入
5 8 10
### 样例输出
65.000000
### *提示*
① 根据题意需要定义存放梯形上底、下底、高和面积的实型变量为：`float supline,dowline,high;`

```
输入：scanf(""%f"",&supline);
     scanf(""%f"",&dowline);
     scanf(""%f"",&high);
```

② 求梯形面积的公式为：面积=(上底+下底)×高/2，即：`area = ((supline+dowline)*high)/2.0;`

③ 为使程序具有通用性，可先从键盘输入上底、下底和高的值，然后再计算面积；

④ 输出计算结果输出语句：`printf("%f \n",area);`

## 参考答案
参见 `1404.c`。
