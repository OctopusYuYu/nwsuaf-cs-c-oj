# nwsuaf-cs-c-oj
西北农林科技大学信息工程学院“C语言程序设计”OJ系统部分题目的参考答案，OJ系统的网址是：202.117.179.201。

<p align="center">
<img src="face.png" width="400" height="222"/>
</p>

## 说明
该项目只是对西北农林科技大学信息工程学院“C语言程序设计”OJ系统部分题目的解答，主要用函数实现。

## 版权声明
该项目由西北农林科技大学信息工程学院计算机科学系耿楠维护。

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />该项目基于 <a rel="license"
href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution
4.0 国际协议</a>.
